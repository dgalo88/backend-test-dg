drop table if exists user;

create table user (
	id_user varchar(36) not null,
	email varchar(50),
	name varchar(50),
	date_created timestamp,
	password varchar(255) not null,
	primary key (id_user),
	unique (id_user)
);

create table campaign (
	id_campaign varchar(36) not null,
	id_user varchar(36),
	subject varchar(255),
	number_of_recipients integer,
	status bit(1),
	date_created timestamp,
	primary key (id_campaign),
	foreign key (id_user) references user(id_user) on delete cascade
);
