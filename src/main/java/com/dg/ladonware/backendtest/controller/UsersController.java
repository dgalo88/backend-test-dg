package com.dg.ladonware.backendtest.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.dg.ladonware.backendtest.dto.CampaignDTO;
import com.dg.ladonware.backendtest.dto.UserDTO;
import com.dg.ladonware.backendtest.exception.ResourceNotFoundException;
import com.dg.ladonware.backendtest.service.ICampaignService;
import com.dg.ladonware.backendtest.service.IUserService;

@RestController
@RequestMapping("/users")
public class UsersController {

	private static final String USER = "User";

	@Autowired
	private IUserService userService;

	@Autowired
	private ICampaignService campaignService;

	@GetMapping
	public ResponseEntity<List<UserDTO>> findAllUsers() {

		List<UserDTO> users = userService.findAll();

		if (users.isEmpty()) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(users);

	}

	@GetMapping("/{id}")
	public ResponseEntity<UserDTO> getUserById(@PathVariable UUID id) {

		Optional<UserDTO> user = userService.findById(id);

		if (!user.isPresent()) {
			throw new ResourceNotFoundException(USER, id);
		}

		return ResponseEntity.ok(user.get());

	}

	@PostMapping
	public ResponseEntity<UserDTO> registerUser(
			@Valid @RequestBody UserDTO user) {

		if (user.getIdUser() == null) {
			user.setIdUser(UUID.randomUUID());
		}

		UserDTO createdUser = userService.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path(
				"/{id}").buildAndExpand(createdUser.getIdUser()).toUri();

		return ResponseEntity.created(location).build();

	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> deleteUser(@PathVariable UUID id) {

		if (userService.deleteById(id)) {
			return ResponseEntity.noContent().build();
		} else {
			throw new ResourceNotFoundException(USER, id);
		}

	}

	@PutMapping
	public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO user) {

		Optional<UserDTO> updatedUser = userService.update(user);

		if (!updatedUser.isPresent()) {
			throw new ResourceNotFoundException(USER, user.getIdUser());
		}

		return ResponseEntity.ok(updatedUser.get());

	}

	@GetMapping("/{id}/campaigns-by-date")
	public ResponseEntity<List<CampaignDTO>> getCampaignHistory(
			@PathVariable UUID id) {

		Optional<UserDTO> user = userService.findById(id);

		List<CampaignDTO> campaigns =
				campaignService.getCampaignHistory(user.get());

		if (campaigns.isEmpty()) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(campaigns);

	}

	@GetMapping("/{id}/campaigns/active/total-emails-sent")
	public ResponseEntity<Long> getTotalEmailsSent(
			@PathVariable UUID id) {

		Optional<UserDTO> user = userService.findById(id);

		if (!user.isPresent()) {
			throw new ResourceNotFoundException(USER, id);
		}

		Long totalEmailsSent = campaignService.getTotalEmailsSent(user.get());
		return ResponseEntity.ok(totalEmailsSent);

	}

	@PostMapping("/{id}/campaigns")
	public ResponseEntity<CampaignDTO> create(
			@PathVariable UUID id,
			@Valid @RequestBody CampaignDTO campaign) {

		Optional<UserDTO> user = userService.findById(id);

		if (!user.isPresent()) {
			throw new ResourceNotFoundException(USER, id);
		}

		if (campaign.getIdCampaign() == null) {
			campaign.setIdCampaign(UUID.randomUUID());
		}

		campaign.setIdUser(id);

		CampaignDTO createdCampaign = campaignService.save(campaign);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path(
				"/{id}").buildAndExpand(createdCampaign.getIdCampaign()).toUri();

		return ResponseEntity.created(location).build();

	}

}
