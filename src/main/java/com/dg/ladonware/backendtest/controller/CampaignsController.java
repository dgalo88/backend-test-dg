package com.dg.ladonware.backendtest.controller;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dg.ladonware.backendtest.dto.CampaignDTO;
import com.dg.ladonware.backendtest.dto.CampaignStatusDTO;
import com.dg.ladonware.backendtest.exception.ResourceNotFoundException;
import com.dg.ladonware.backendtest.service.ICampaignService;

@RestController
@RequestMapping("/campaigns")
public class CampaignsController {

	private static final String CAMPAIGN = "Campaign";

	@Autowired
	private ICampaignService service;

	@PutMapping("/{id}/update-status")
	public ResponseEntity<CampaignDTO> updateStatus(
			@PathVariable UUID id,
			@RequestBody CampaignStatusDTO campaign) {

		Optional<CampaignDTO> updatedCampaign = service.update(
				id, campaign.getStatus() == 1 ?
						Boolean.TRUE : Boolean.FALSE);

		if (!updatedCampaign.isPresent()) {
			throw new ResourceNotFoundException(CAMPAIGN, id);
		}

		return ResponseEntity.ok(updatedCampaign.get());

	}

}

