package com.dg.ladonware.backendtest.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dg.ladonware.backendtest.model.UserEntity;

public interface IUserRepository extends JpaRepository<UserEntity, UUID> {

	UserEntity findByEmail(String email);

}
