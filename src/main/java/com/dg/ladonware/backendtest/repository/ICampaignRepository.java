package com.dg.ladonware.backendtest.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.dg.ladonware.backendtest.model.CampaignEntity;
import com.dg.ladonware.backendtest.model.UserEntity;

public interface ICampaignRepository extends JpaRepository<CampaignEntity, UUID> {

	public List<CampaignEntity> findByUserOrderByDateCreatedDesc(UserEntity user);

	@Query("select sum(c.numberOfRecipients) from CampaignEntity c where c.user = ?1 and c.status = '1'")
	public Long sumNumberOfRecipientsByUser(UserEntity user);

}
