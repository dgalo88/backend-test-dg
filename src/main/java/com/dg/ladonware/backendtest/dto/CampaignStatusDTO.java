package com.dg.ladonware.backendtest.dto;

public class CampaignStatusDTO {

	private Integer status;

	public CampaignStatusDTO() {
		super();
	}

	public CampaignStatusDTO(Integer status) {
		super();
		this.status = status;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return String.format("CampaignStatusDTO [status=%s]", status);
	}

}
