package com.dg.ladonware.backendtest.dto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class UserDTO {

	private UUID idUser;

	@NotNull
	@NotEmpty
	@Email
	private String email;

	private String name;

	@PastOrPresent
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dateCreated;

	@NotNull
	@NotEmpty
	private String password;

	private List<CampaignDTO> campaigns;

	public UserDTO() {
		super();
	}

	public UserDTO(UUID idUser, String email,
			String name, LocalDateTime dateCreated,
			String password, List<CampaignDTO> campaigns) {
		super();
		this.idUser = idUser;
		this.email = email;
		this.name = name;
		this.dateCreated = dateCreated;
		this.password = password;
		this.campaigns = campaigns;
	}

	public UUID getIdUser() {
		return idUser;
	}

	public void setIdUser(UUID idUser) {
		this.idUser = idUser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<CampaignDTO> getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(List<CampaignDTO> campaigns) {
		this.campaigns = campaigns;
	}

	@Override
	public String toString() {
		return String.format("UserDTO [idUser=%s, email=%s, name=%s, dateCreated=%s, campaigns=%s]",
				idUser, email, name, dateCreated, campaigns);
	}

}
