package com.dg.ladonware.backendtest.dto;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

public class CampaignDTO {

	private UUID idCampaign;

	@NotNull
	@NotEmpty
	private UUID idUser;

	private String subject;

	private Integer numberOfRecipients;

	private Integer status;

	@PastOrPresent
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dateCreated;

	public CampaignDTO() {
		super();
	}

	public CampaignDTO(UUID idCampaign, UUID idUser,
			String subject, Integer numberOfRecipients,
			Integer status, LocalDateTime dateCreated) {
		super();
		this.idCampaign = idCampaign;
		this.idUser = idUser;
		this.subject = subject;
		this.numberOfRecipients = numberOfRecipients;
		this.status = status;
		this.dateCreated = dateCreated;
	}

	public UUID getIdCampaign() {
		return idCampaign;
	}

	public void setIdCampaign(UUID idCampaign) {
		this.idCampaign = idCampaign;
	}

	public UUID getIdUser() {
		return idUser;
	}

	public void setIdUser(UUID idUser) {
		this.idUser = idUser;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Integer getNumberOfRecipients() {
		return numberOfRecipients;
	}

	public void setNumberOfRecipients(Integer numberOfRecipients) {
		this.numberOfRecipients = numberOfRecipients;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public String toString() {
		return String.format(
				"CampaignDTO [idCampaign=%s, idUser=%s, subject=%s, numberOfRecipients=%s, status=%s, dateCreated=%s]",
				idCampaign, idUser, subject, numberOfRecipients, status, dateCreated);
	}

}
