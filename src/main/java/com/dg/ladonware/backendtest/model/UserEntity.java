package com.dg.ladonware.backendtest.model;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "user")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserEntity {

	@Id
	@Column(name = "id_user")
	@Type(type = "uuid-char")
	private UUID idUser;

	@NotNull
	@NotEmpty
	@Email
	private String email;

	private String name;

	@Column(name = "date_created")
	@PastOrPresent
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dateCreated;

	@NotNull
	@NotEmpty
	private String password;

	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
	@JoinColumn(name = "id_user")
	private List<CampaignEntity> campaigns;

	public UserEntity() {
		super();
	}

	public UserEntity(UUID idUser, String email,
			String name, LocalDateTime dateCreated,
			List<CampaignEntity> campaigns) {
		super();
		this.idUser = idUser;
		this.email = email;
		this.name = name;
		this.dateCreated = dateCreated;
		this.campaigns = campaigns;
	}

	public UUID getIdUser() {
		return idUser;
	}

	public void setIdUser(UUID idUser) {
		this.idUser = idUser;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public List<CampaignEntity> getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(List<CampaignEntity> campaigns) {
		this.campaigns = campaigns;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return String.format("UserEntity [idUser=%s, email=%s, name=%s, dateCreated=%s]",
				idUser, email, name, dateCreated);
	}

}
