package com.dg.ladonware.backendtest.model;

import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.PastOrPresent;

import org.hibernate.annotations.Type;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "campaign")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CampaignEntity {

	@Id
	@Column(name = "id_campaign")
	@Type(type = "uuid-char")
	private UUID idCampaign;

	private String subject;

	@Column(name = "number_of_recipients")
	private Integer numberOfRecipients;

	@Type(type = "org.hibernate.type.NumericBooleanType")
	private Boolean status;

	@Column(name = "date_created")
	@PastOrPresent
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime dateCreated;

	@ManyToOne
	@JoinColumn(name = "id_user")
	private UserEntity user;

	public CampaignEntity() {
		super();
	}

	public CampaignEntity(UUID idCampaign, String subject,
			Integer numberOfRecipients, Boolean status,
			LocalDateTime dateCreated, UserEntity user) {
		super();
		this.idCampaign = idCampaign;
		this.subject = subject;
		this.numberOfRecipients = numberOfRecipients;
		this.status = status;
		this.dateCreated = dateCreated;
		this.user = user;
	}

	public UUID getIdCampaign() {
		return idCampaign;
	}

	public void setIdCampaign(UUID idCampaign) {
		this.idCampaign = idCampaign;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Integer getNumberOfRecipients() {
		return numberOfRecipients;
	}

	public void setNumberOfRecipients(Integer numberOfRecipients) {
		this.numberOfRecipients = numberOfRecipients;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public LocalDateTime getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(LocalDateTime dateCreated) {
		this.dateCreated = dateCreated;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return String.format(
				"CampaignEntity [idCampaign=%s, subject=%s, numberOfRecipients=%s, status=%s, dateCreated=%s, user=%s]",
				idCampaign, subject, numberOfRecipients, status, dateCreated, user);
	}

}
