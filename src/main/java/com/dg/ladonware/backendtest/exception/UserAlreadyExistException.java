package com.dg.ladonware.backendtest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class UserAlreadyExistException extends RuntimeException {

	private static final long serialVersionUID = -6871806099020423394L;

	public UserAlreadyExistException(String email) {
		super(String.format("User '%s' already exists", email));
	}

}
