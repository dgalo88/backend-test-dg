package com.dg.ladonware.backendtest.exception;

import java.time.LocalDateTime;
import java.util.List;

public class ExceptionResponse {

	private LocalDateTime timestamp;
	private String message;
	private List<String> errors;

	public ExceptionResponse(LocalDateTime timestamp,
			String message, List<String> errors) {
		super();
		this.timestamp = timestamp;
		this.message = message;
		this.errors = errors;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public String getMessage() {
		return message;
	}

	public List<String> getErrors() {
		return errors;
	}

}
