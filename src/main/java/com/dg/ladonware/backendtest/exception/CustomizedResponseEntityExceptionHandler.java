package com.dg.ladonware.backendtest.exception;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(
			Exception ex, WebRequest request) throws Exception {

		ExceptionResponse response = new ExceptionResponse(
				LocalDateTime.now(), ex.getMessage(),
				Arrays.asList(new String[] { request.getDescription(false) }));

		return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);

	}

	@ExceptionHandler(ResourceNotFoundException.class)
	public final ResponseEntity<Object> handleResourceNotFoundException(
			ResourceNotFoundException ex, WebRequest request) throws Exception {

		ExceptionResponse response = new ExceptionResponse(
				LocalDateTime.now(), ex.getMessage(),
				Arrays.asList(new String[] { request.getDescription(false) }));

		return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);

	}

	@ExceptionHandler(UserAlreadyExistException.class)
	public final ResponseEntity<Object> handleUserAlreadyExistException(
			UserAlreadyExistException ex, WebRequest request) throws Exception {

		ExceptionResponse response = new ExceptionResponse(
				LocalDateTime.now(), ex.getMessage(),
				Arrays.asList(new String[] { request.getDescription(false) }));

		return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);

	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		List<String> errors = new ArrayList<>();

		for (FieldError fieldError : ex.getFieldErrors()) {
			errors.add(String.format("%s %s",
					fieldError.getField(),
					fieldError.getDefaultMessage()));
		}

		ExceptionResponse response = new ExceptionResponse(
				LocalDateTime.now(), "Validation failed", errors);

		return handleExceptionInternal(ex, response, headers, status, request);

	}

}
