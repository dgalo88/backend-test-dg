package com.dg.ladonware.backendtest.client.service;

import com.dg.ladonware.backendtest.dto.UserDTO;

public interface IUserConsumerService {

	public UserDTO registerUser(UserDTO user);

	public Long getTotalEmailsSent(UserDTO user);

}
