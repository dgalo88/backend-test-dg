package com.dg.ladonware.backendtest.client.controller;

import java.io.IOException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;

import com.dg.ladonware.backendtest.client.service.IUserConsumerService;
import com.dg.ladonware.backendtest.dto.MessageDTO;
import com.dg.ladonware.backendtest.dto.UserDTO;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
@RequestMapping("/")
public class UserConsumerController {

	@Autowired
	private IUserConsumerService service;

	@Autowired
	private ObjectMapper mapper;

	@GetMapping("/registration")
	public String showRegistrationForm(WebRequest request, Model model) {
		UserDTO userDTO = new UserDTO();
		model.addAttribute("user", userDTO);
		return "registration";
	}

	@PostMapping("/app/user/registration")
	public String registerUser(Model model,
			@ModelAttribute("user") @Valid UserDTO user) {

		try {
			UserDTO registeredUser = service.registerUser(user);
		} catch (Exception e) {
			model.addAttribute("error", "User registration failed");
			return "redirect:/registration";
		}

		return "redirect:/login";

	}

	@GetMapping("/login")
	public String showLoginForm(WebRequest request, Model model) {
		UserDTO userDTO = new UserDTO();
		model.addAttribute("user", userDTO);
		return "login";
	}

	@GetMapping("/homepage")
	public String showHomePage(WebRequest request, Model model) {
		UserDTO userDTO = new UserDTO();
		model.addAttribute("user", userDTO);
		return "homepage";
	}

	//	@GetMapping
	//	public String findAll(Model model) {
	//		model.addAttribute("users", service.getTotalEmailsSent(user));
	//		model.addAttribute("newUser", new UserDTO());
	//		return "users";
	//	}

	//	@GetMapping
	//	public String findAll(Model model) {
	//		model.addAttribute("users", service.findAll());
	//		model.addAttribute("newUser", new UserDTO());
	//		return "users";
	//	}
	//
	//	@PutMapping
	//	public String update(@RequestParam Long id, UserDTO user) {
	//		service.update(id, user);
	//		return "redirect:/";
	//	}
	//
	//	@DeleteMapping
	//	public String delete(@RequestParam Long id) {
	//		service.delete(id);
	//		return "redirect:/";
	//	}

	@ExceptionHandler(HttpClientErrorException.class)
	public String handleClientError(HttpClientErrorException ex,
			WebRequest request, Model model) throws IOException {
		MessageDTO dto = mapper.readValue(ex.getResponseBodyAsByteArray(), MessageDTO.class);
		model.addAttribute("error", dto.getMessage());
		return "redirect:/login";
	}

}
