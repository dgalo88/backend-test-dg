package com.dg.ladonware.backendtest.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dg.ladonware.backendtest.dto.CampaignDTO;
import com.dg.ladonware.backendtest.dto.UserDTO;
import com.dg.ladonware.backendtest.model.CampaignEntity;
import com.dg.ladonware.backendtest.model.UserEntity;
import com.dg.ladonware.backendtest.repository.ICampaignRepository;
import com.dg.ladonware.backendtest.util.Util;

@Service
public class CampaignServiceImpl implements ICampaignService {

	@Autowired
	private ICampaignRepository repository;

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private Util util;

	@Override
	public CampaignDTO save(CampaignDTO campaign) {

		CampaignEntity campaignEntity = mapper.map(campaign, CampaignEntity.class);
		CampaignEntity savedCampaign = repository.save(campaignEntity);

		return mapper.map(savedCampaign, CampaignDTO.class);

	}

	@Override
	public Optional<CampaignDTO> update(UUID id, Boolean status) {

		Optional<CampaignEntity> optCampaignEntity = repository.findById(id);

		if (optCampaignEntity.isPresent()) {

			CampaignEntity campaignEntity = optCampaignEntity.get();
			campaignEntity.setStatus(status);

			repository.save(campaignEntity);

			return Optional.ofNullable(mapper.map(
					campaignEntity, CampaignDTO.class));

		}

		return Optional.empty();

	}

	@Override
	public List<CampaignDTO> getCampaignHistory(UserDTO user) {
		UserEntity userEntity = mapper.map(user, UserEntity.class);
		List<CampaignEntity> campaigns = repository.findByUserOrderByDateCreatedDesc(userEntity);
		return util.mapList(campaigns, CampaignDTO.class);
	}

	@Override
	public Long getTotalEmailsSent(UserDTO user) {
		UserEntity userEntity = mapper.map(user, UserEntity.class);
		return repository.sumNumberOfRecipientsByUser(userEntity);
	}

}
