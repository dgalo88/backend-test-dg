package com.dg.ladonware.backendtest.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.dg.ladonware.backendtest.dto.UserDTO;
import com.dg.ladonware.backendtest.exception.UserAlreadyExistException;
import com.dg.ladonware.backendtest.model.UserEntity;
import com.dg.ladonware.backendtest.repository.IUserRepository;
import com.dg.ladonware.backendtest.util.Util;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private IUserRepository repository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private ModelMapper mapper;

	@Autowired
	private Util util;

	private boolean userExist(String email) {
		return repository.findByEmail(email) != null;
	}

	@Override
	public List<UserDTO> findAll() {
		List<UserEntity> users = repository.findAll();
		List<UserDTO> usersDTO = util.mapList(users, UserDTO.class);
		return usersDTO;
	}

	@Override
	public Optional<UserDTO> findById(UUID id) {
		Optional<UserEntity> user = repository.findById(id);
		return Optional.of(mapper.map(user.get(), UserDTO.class));
	}

	@Override
	public UserDTO save(UserDTO user) throws UserAlreadyExistException {

		if (userExist(user.getEmail())) {
			throw new UserAlreadyExistException(user.getEmail());
		}

		user.setPassword(passwordEncoder.encode(user.getPassword()));

		UserEntity userEntity = mapper.map(user, UserEntity.class);
		UserEntity savedUser = repository.save(userEntity);

		return mapper.map(savedUser, UserDTO.class);

	}

	@Override
	public boolean deleteById(UUID id) {

		if (repository.findById(id).isPresent()) {
			repository.deleteById(id);
			return true;
		}

		return false;

	}

	@Override
	public Optional<UserDTO> update(UserDTO user) {

		UUID id = user.getIdUser();

		if (repository.findById(id).isPresent()) {

			UserEntity updatedUser = new UserEntity();

			updatedUser.setIdUser(id);
			updatedUser.setEmail(user.getEmail());
			updatedUser.setName(user.getName());
			updatedUser.setDateCreated(user.getDateCreated());

			repository.save(updatedUser);

			return Optional.ofNullable(mapper.map(updatedUser, UserDTO.class));

		}

		return Optional.empty();

	}

}
