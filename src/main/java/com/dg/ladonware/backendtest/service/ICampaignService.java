package com.dg.ladonware.backendtest.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.dg.ladonware.backendtest.dto.CampaignDTO;
import com.dg.ladonware.backendtest.dto.UserDTO;

public interface ICampaignService {

	public CampaignDTO save(CampaignDTO campaign);

	public Optional<CampaignDTO> update(UUID id, Boolean status);

	public List<CampaignDTO> getCampaignHistory(UserDTO user);

	public Long getTotalEmailsSent(UserDTO user);

}
