package com.dg.ladonware.backendtest.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import com.dg.ladonware.backendtest.dto.UserDTO;
import com.dg.ladonware.backendtest.exception.UserAlreadyExistException;

public interface IUserService {

	public List<UserDTO> findAll();

	public Optional<UserDTO> findById(UUID id);

	public UserDTO save(UserDTO user) throws UserAlreadyExistException;

	public boolean deleteById(UUID id);

	public Optional<UserDTO> update(UserDTO user);

}
